package main

import (
	"github.com/VolantMQ/vlapi/vlmonitoring"
	"github.com/VolantMQ/vlapi/vltypes"
)

type metricEntry struct {
	sent vlmonitoring.DynamicIFace
	recv vlmonitoring.DynamicIFace
}

type loadMetric struct {
	min1  vlmonitoring.DynamicIFace
	min5  vlmonitoring.DynamicIFace
	min15 vlmonitoring.DynamicIFace
}

type packetsMetric struct {
	total            *metricEntry
	storedCount      vlmonitoring.DynamicIFace // deprecated
	stored           vlmonitoring.DynamicIFace
	inflight         vlmonitoring.DynamicIFace
	retainedCount    vlmonitoring.DynamicIFace
	retainedMax      vlmonitoring.DynamicIFace
	connect          *metricEntry
	connAck          *metricEntry
	publish          *metricEntry
	puback           *metricEntry // nolint structcheck
	pubrec           *metricEntry // nolint structcheck
	pubrel           *metricEntry // nolint structcheck
	pubcomp          *metricEntry // nolint structcheck
	subscribe        *metricEntry
	suback           *metricEntry
	unsubscribe      *metricEntry
	unSubAck         *metricEntry
	pingReq          *metricEntry
	pingResp         *metricEntry
	disconnect       *metricEntry
	auth             *metricEntry
	loadMessagesSent *loadMetric
	loadMessagesRecv *loadMetric
	loadPublishSent  *loadMetric
	loadPublishRecv  *loadMetric
}

type bytesMetric struct {
	metricEntry
}

type metricBytes struct {
	bytes    *bytesMetric
	loadSent *loadMetric
	loadRecv *loadMetric
}

type metric struct {
	bytes              *metricBytes
	packets            *packetsMetric
	subscriptionsCount vlmonitoring.DynamicIFace
	subscriptionsMax   vlmonitoring.DynamicIFace
	connections        *loadMetric
}

func newLoadMetric(topicPrefix string, retained *[]vltypes.RetainObject) *loadMetric {
	lm := &loadMetric{
		min1:  newDynamicValueIntegerMVA(topicPrefix+"/1min", 60, 60),
		min5:  newDynamicValueIntegerMVA(topicPrefix+"/5min", 300, 60),
		min15: newDynamicValueIntegerMVA(topicPrefix+"/15min", 900, 60),
	}

	*retained = append(*retained, lm.min1, lm.min5, lm.min15)

	return lm
}

func newMetricEntry(topicPrefix string, retained *[]vltypes.RetainObject) *metricEntry {
	m := &metricEntry{
		sent: newDynamicValueInteger(topicPrefix + "/sent"),
		recv: newDynamicValueInteger(topicPrefix + "/received"),
	}

	*retained = append(*retained, m.sent, m.recv)
	return m
}

func newSoloEntry(topicPrefix string, retained *[]vltypes.RetainObject) vlmonitoring.DynamicIFace {
	m := newDynamicValueInteger(topicPrefix)
	*retained = append(*retained, m)
	return m
}

func newBytesMetric(topicPrefix string, retained *[]vltypes.RetainObject) *bytesMetric {
	return &bytesMetric{
		metricEntry: *newMetricEntry(topicPrefix+"/bytes", retained),
	}
}

func newMetric(topicPrefix string, retained *[]vltypes.RetainObject) metric {
	return metric{
		packets: newPacketsMetric(topicPrefix, retained),
		bytes: &metricBytes{
			bytes:    newBytesMetric(topicPrefix, retained),
			loadSent: newLoadMetric(topicPrefix+"/load/bytes/sent", retained),
			loadRecv: newLoadMetric(topicPrefix+"/load/bytes/received", retained),
		},
		subscriptionsCount: newSoloEntry(topicPrefix+"/subscriptions/count", retained),
		subscriptionsMax:   newSoloEntry(topicPrefix+"/subscriptions/max", retained),
		connections:        newLoadMetric(topicPrefix+"/load/connections", retained),
	}
}

func newPacketsMetric(topicPrefix string, retained *[]vltypes.RetainObject) *packetsMetric {
	return &packetsMetric{
		total:            newMetricEntry(topicPrefix+"/messages", retained),
		stored:           newSoloEntry(topicPrefix+"/store/messages/count", retained),
		storedCount:      newSoloEntry(topicPrefix+"/messages/stored", retained),
		inflight:         newSoloEntry(topicPrefix+"/messages/inflight", retained),
		retainedCount:    newSoloEntry(topicPrefix+"/retained/messages/count", retained),
		retainedMax:      newSoloEntry(topicPrefix+"/retained/messages/max", retained),
		connect:          newMetricEntry(topicPrefix+"/messages/connect", retained),
		connAck:          newMetricEntry(topicPrefix+"/messages/connack", retained),
		publish:          newMetricEntry(topicPrefix+"/messages/publish", retained),
		subscribe:        newMetricEntry(topicPrefix+"/messages/subscribe", retained),
		suback:           newMetricEntry(topicPrefix+"/messages/suback", retained),
		unsubscribe:      newMetricEntry(topicPrefix+"/messages/unsubscribe", retained),
		unSubAck:         newMetricEntry(topicPrefix+"/messages/unsuback", retained),
		pingReq:          newMetricEntry(topicPrefix+"/messages/pingreq", retained),
		pingResp:         newMetricEntry(topicPrefix+"/messages/pingresp", retained),
		disconnect:       newMetricEntry(topicPrefix+"/messages/disconnect", retained),
		auth:             newMetricEntry(topicPrefix+"/messages/auth", retained),
		loadMessagesSent: newLoadMetric(topicPrefix+"/load/messages/sent", retained),
		loadMessagesRecv: newLoadMetric(topicPrefix+"/load/messages/received", retained),
		loadPublishSent:  newLoadMetric(topicPrefix+"/load/publish/sent", retained),
		loadPublishRecv:  newLoadMetric(topicPrefix+"/load/publish/received", retained),
	}
}

// Sent add sent bytes to statistic
func (t *metricBytes) Sent(bytes uint64) {
	t.bytes.sent.Add(bytes)
}

// Received add received bytes to statistic
func (t *metricBytes) Recv(bytes uint64) {
	t.bytes.recv.Add(bytes)
}

// Sent add sent bytes to statistic
func (t *bytesMetric) Sent(bytes uint64) {
	t.sent.Add(bytes)
}

// Received add received bytes to statistic
func (t *bytesMetric) Recv(bytes uint64) {
	t.recv.Add(bytes)
}
