package main

import (
	"encoding/json"

	"github.com/VolantMQ/vlapi/mqttp"
	"github.com/VolantMQ/vlapi/vlmonitoring"
	"github.com/VolantMQ/vlapi/vltypes"
)

type server struct {
	upTime   vlmonitoring.DynamicIFace
	currTime vlmonitoring.DynamicIFace
	load     struct { // nolint structcheck
		bytes struct {
			received dynamicValueInteger
			sent     dynamicValueInteger
		}
	}
	capabilities struct {
		SupportedVersions             []mqttp.ProtocolVersion
		MaxQoS                        string
		MaxConnections                uint64
		MaximumPacketSize             uint32
		ServerKeepAlive               uint16
		ReceiveMaximum                uint16
		RetainAvailable               bool
		WildcardSubscriptionAvailable bool
		SubscriptionIDAvailable       bool
		SharedSubscriptionAvailable   bool
	}
}

func newServer(topicPrefix, ver, build string, dynRetains, staticRetains *[]vltypes.RetainObject) server {
	b := server{
		upTime:   newDynamicValueUpTime(topicPrefix + "/uptime"),
		currTime: newDynamicValueCurrentTime(topicPrefix + "/datetime"),
	}

	m, _ := mqttp.New(mqttp.ProtocolV311, mqttp.PUBLISH)
	msg, _ := m.(*mqttp.Publish)
	_ = msg.SetQoS(mqttp.QoS0)
	_ = msg.SetTopic(topicPrefix + "/version")
	msg.SetPayload([]byte(ver))
	*staticRetains = append(*staticRetains, msg)

	m, _ = mqttp.New(mqttp.ProtocolV311, mqttp.PUBLISH)
	msg, _ = m.(*mqttp.Publish)
	_ = msg.SetQoS(mqttp.QoS0)
	_ = msg.SetTopic(topicPrefix + "/timestamp")
	msg.SetPayload([]byte(build))
	*staticRetains = append(*staticRetains, msg)

	*dynRetains = append(*dynRetains, b.upTime)
	*dynRetains = append(*dynRetains, b.currTime)

	m, _ = mqttp.New(mqttp.ProtocolV311, mqttp.PUBLISH)
	msg, _ = m.(*mqttp.Publish)
	_ = msg.SetQoS(mqttp.QoS0)
	_ = msg.SetTopic(topicPrefix + "/capabilities")

	if data, err := json.Marshal(&b.capabilities); err == nil {
		msg.SetPayload(data)
	} else {
		msg.SetPayload([]byte(err.Error()))
	}

	*staticRetains = append(*staticRetains, msg)

	return b
}
