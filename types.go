package main

import (
	"fmt"
	"math"
	"strconv"
	"sync/atomic"
	"time"

	"github.com/VolantMQ/vlapi/mqttp"
	"github.com/VolantMQ/vlapi/vlmonitoring"
)

type dynamicValue struct {
	topic    string
	retained *mqttp.Publish
	publish  *mqttp.Publish
	value    func() []byte
}

func (m *dynamicValue) Add(n uint64) uint64 { return 0 }
func (m *dynamicValue) Set(n uint64)        {}
func (m *dynamicValue) Get() uint64         { return 0 }

type dynamicValueInteger struct {
	val uint64
	dynamicValue
}

type dynamicValueMVA struct {
	val        uint64
	prev       uint64
	interval   float64
	window     float64
	lastUpdate time.Time
	dynamicValue
}

type dynamicValueUpTime struct {
	dynamicValue
	startTime time.Time
}

type dynamicValueCurrentTime struct {
	dynamicValue
}

var _ vlmonitoring.DynamicIFace = (*dynamicValueInteger)(nil)
var _ vlmonitoring.DynamicIFace = (*dynamicValueInteger)(nil)
var _ vlmonitoring.DynamicIFace = (*dynamicValueInteger)(nil)
var _ vlmonitoring.DynamicIFace = (*dynamicValueCurrentTime)(nil)

func newDynamicValueInteger(topic string) vlmonitoring.DynamicIFace {
	v := &dynamicValueInteger{}
	v.topic = topic
	v.value = v.get

	return v
}

func newDynamicValueIntegerMVA(topic string, interval, window int) vlmonitoring.DynamicIFace {
	v := &dynamicValueMVA{
		interval: float64(interval),
		window:   float64(window),
	}

	v.topic = topic
	v.value = v.get

	return v
}

func newDynamicValueUpTime(topic string) vlmonitoring.DynamicIFace {
	v := &dynamicValueUpTime{
		startTime: time.Now(),
	}

	v.topic = topic
	v.value = v.get

	return v
}

func newDynamicValueCurrentTime(topic string) vlmonitoring.DynamicIFace {
	v := &dynamicValueCurrentTime{}
	v.topic = topic
	v.value = v.get

	return v
}

func (v *dynamicValueInteger) get() []byte {
	val := strconv.FormatUint(atomic.LoadUint64(&v.val), 10)
	return []byte(val)
}

func (v *dynamicValueInteger) Add(n uint64) uint64 {
	return atomic.AddUint64(&v.val, n)
}

func (v *dynamicValueMVA) Add(n uint64) uint64 {
	return atomic.AddUint64(&v.val, n)
}

func (v *dynamicValueInteger) Set(n uint64) {
	atomic.StoreUint64(&v.val, n)
}

func (v *dynamicValueMVA) Set(n uint64) {
	atomic.StoreUint64(&v.val, n)
}

func (v *dynamicValueInteger) Get() uint64 {
	return atomic.LoadUint64(&v.val)
}

func (v *dynamicValueMVA) Get() uint64 {
	return atomic.LoadUint64(&v.val)
}

func (v *dynamicValueMVA) get() []byte {
	var val float64
	curr := atomic.LoadUint64(&v.val)
	prev := atomic.LoadUint64(&v.prev)
	atomic.StoreUint64(&v.prev, curr)

	if !v.lastUpdate.IsZero() {
		now := time.Now()
		tsDiff := now.Sub(v.lastUpdate).Seconds()
		iMul := v.window / tsDiff

		valInterval := (float64(curr)-float64(prev)) * iMul

		exponent := math.Exp(-1.0 * tsDiff / v.interval)

		val = valInterval + exponent*(float64(curr)-valInterval)
		v.lastUpdate = now

		if math.Abs(val-float64(curr)) >= 0.01 {
			return []byte(fmt.Sprintf("%.2f", val))
		}
	} else {
		return []byte(fmt.Sprintf("%.2f", float64(curr)))
	}

	return []byte{}
}

func (v *dynamicValueUpTime) get() []byte {
	diff := time.Since(v.startTime)

	return []byte(diff.String())
}

func (v *dynamicValueCurrentTime) get() []byte {
	val := time.Now().Format(time.RFC3339)
	return []byte(val)
}

// Topic returns topic
func (m *dynamicValue) Topic() string {
	return m.topic
}

func (m *dynamicValue) Retained() *mqttp.Publish {
	if m.retained == nil {
		np, _ := mqttp.New(mqttp.ProtocolV311, mqttp.PUBLISH)
		m.retained, _ = np.(*mqttp.Publish)
		_ = m.retained.SetTopic(m.topic)
		_ = m.retained.SetQoS(mqttp.QoS0)
		m.retained.SetRetain(true)
	}

	if buf := m.value(); len(buf) > 0 {
		m.retained.SetPayload(buf)
		return m.retained
	}

	return nil
}

func (m *dynamicValue) Publish() *mqttp.Publish {
	if m.publish == nil {
		np, _ := mqttp.New(mqttp.ProtocolV311, mqttp.PUBLISH)
		m.publish, _ = np.(*mqttp.Publish)
		_ = m.publish.SetTopic(m.topic)
		_ = m.publish.SetQoS(mqttp.QoS0)
		m.publish.SetRetain(true)
	}

	if buf := m.value(); len(buf) > 0 {
		m.publish.SetPayload(buf)
		return m.publish
	}

	return nil
}
