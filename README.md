# SYS Topics
[![pipeline status](https://gitlab.com/volantmq/vlplugin/monitoring/systree/badges/master/pipeline.svg)](https://gitlab.com/volantmq/vlplugin/monitoring/systree/commits/master)

Most of the topics and descriptions are copied and modified from: http://mosquitto.org/man/mosquitto-8.html

## Config
Docker image of VolantMQ service comes with systree plugin
To enable plugin add `systree` value to list of enabled plugins. In config section specify update interval
```yaml
plugins:
  enabled:
    - systree
  config:
    monitoring:
      - backend: systree
        config:
          interval: 1
```
## Reported Topics
- **$SYS/broker/clients/connected**: The number of currently connected clients<br/>
  **$SYS/broker/clients/active**: `deprecated`
- **$SYS/broker/clients/disconnected**: The total number of persistent clients (with clean session disabled) that are registered at the broker but are currently disconnected.<br/>
  **$SYS/broker/clients/inactive**: `deprecated`
- **$SYS/broker/clients/maximum**: The maximum number of active clients that have been connected to the broker. This is only calculated when the $SYS topic tree is updated, so short lived client connections may not be counted.
- **$SYS/broker/clients/total**: The total number of connected and disconnected clients with a persistent session currently connected and registered on the broker.
- **$SYS/broker/clients/expired**: The number of disconnected persistent clients that have been expired

- **$SYS/broker/load/bytes/received**: The total number of bytes received since the broker started.
- **$SYS/broker/load/bytes/sent**: The total number of bytes sent since the broker started.
- **$SYS/broker/messages/inflight**: The number of messages with QoS>0 that are awaiting acknowledgments.
- **$SYS/broker/retained/messages/count**: The total number of retained messages active on the broker.
- **$SYS/broker/retained/messages/max**: The maximum number of retained messages even been on server.
- **$SYS/broker/store/messages/count**: The number of messages currently held in the message store. This includes retained messages and messages queued for durable clients.<br/>
  **$SYS/broker/messages/stored**: `deprecated`
- **$SYS/broker/subscriptions/count**: The total number of subscriptions active on the broker.
- **$SYS/broker/subscriptions/max**: The maximum number of subscriptions active on the broker.
- **$SYS/broker/messages/received**: The total number of messages of any type received since the broker started.
- **$SYS/broker/messages/sent**: The total number of messages of any type sent since the broker started.
- **$SYS/broker/messages/publish/dropped**: The total number of publish messages that have been dropped due to inflight/queuing limits.
- **$SYS/broker/messages/publish/received**: The total number of PUBLISH messages received since the broker started.
- **$SYS/broker/messages/publish/sent**: The total number of PUBLISH messages sent since the broker started.
- **$SYS/broker/bytes/sent**: The total number of bytes sent since the broker started.
- **$SYS/broker/bytes/received**: The total number of bytes received since the broker started.
- **$SYS/broker/load/connections/+**: The moving average of the number of CONNECT packets received by the broker over different time intervals. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of connections received in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/load/bytes/received/+**: The moving average of the number of bytes received by the broker over different time intervals. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of bytes received in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/load/bytes/sent/+**: The moving average of the number of bytes sent by the broker over different time intervals. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of bytes sent in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/load/messages/received/+**: The moving average of the number of all types of MQTT messages received by the broker over different time intervals. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of messages received in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/load/messages/sent/+**: The moving average of the number of all types of MQTT messages sent by the broker over different time intervals. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of messages send in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/load/publish/dropped/+**: The moving average of the number of publish messages dropped by the broker over different time intervals. This shows the rate at which durable clients that are disconnected are losing messages. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of messages dropped in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/load/publish/received/+**: The moving average of the number of publish messages received by the broker over different time intervals. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of publish messages received in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/load/publish/sent/+**: The moving average of the number of publish messages sent by the broker over different time intervals. The final "+" of the hierarchy can be 1min, 5min or 15min. The value returned represents the number of publish messages sent in 1 minute, averaged over 1, 5 or 15 minutes.
- **$SYS/broker/uptime**: The amount of time in seconds the broker has been online.
- **$SYS/broker/version**: The version of the broker. Static.
