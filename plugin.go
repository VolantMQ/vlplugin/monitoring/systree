package main

import (
	"github.com/VolantMQ/vlapi/vlplugin"
)

type systreePlugin struct {
	vlplugin.Descriptor
}

var _ vlplugin.Plugin = (*systreePlugin)(nil)
var _ vlplugin.Info = (*systreePlugin)(nil)

// Plugin symbol
var Plugin systreePlugin

var version string

func init() {
	Plugin.V = version
	Plugin.N = "systree"
	Plugin.T = "monitoring"
}

// Info plugin info
func (pl *systreePlugin) Info() vlplugin.Info {
	return pl
}

func main() {
	panic("this is a plugin, build it as with -buildmode=plugin")
}
