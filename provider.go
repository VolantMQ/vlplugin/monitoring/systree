package main

import (
	"fmt"
	"reflect"
	"sync"
	"time"

	"github.com/VolantMQ/vlapi/mqttp"
	"github.com/VolantMQ/vlapi/vlmonitoring"
	"github.com/VolantMQ/vlapi/vlplugin"
	"github.com/VolantMQ/vlapi/vltypes"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Interval int `json:"interval"`
}

type tree struct {
	server  server
	metric  metric
	clients clients
}
type impl struct {
	*vlplugin.SysParams
	config    Config
	publishes []vlmonitoring.DynamicIFace
	quit      chan struct{}
	wgStarted sync.WaitGroup
	wgStopped sync.WaitGroup
	timer     *time.Timer
	tree      tree
}

var _ vlmonitoring.IFace = (*impl)(nil)

func (pl *systreePlugin) Load(c interface{}, params *vlplugin.SysParams) (interface{}, error) {
	var err error

	var cfg Config
	decodeIFace := func() error {
		var data []byte
		var e error
		if data, e = yaml.Marshal(c); e != nil {
			e = errors.New(Plugin.T + "." + Plugin.N + ": " + e.Error())
			return e
		}

		if e = yaml.Unmarshal(data, &cfg); e != nil {
			e = errors.New(Plugin.T + "." + Plugin.N + ": " + e.Error())
			return e
		}

		return e
	}

	switch t := c.(type) {
	case map[string]interface{}:
		if err = decodeIFace(); err != nil {
			return nil, err
		}
	case map[interface{}]interface{}:
		if err = decodeIFace(); err != nil {
			return nil, err
		}
	case []byte:
		if err = yaml.Unmarshal(t, &cfg); err != nil {
			err = errors.New(Plugin.T + "." + Plugin.N + ": " + err.Error())
			return nil, err
		}
	default:
		err = fmt.Errorf("%s.%s: invalid config type %s", Plugin.T, Plugin.N, reflect.TypeOf(c).String())
		return nil, err
	}

	var retains []vltypes.RetainObject
	var staticRetains []vltypes.RetainObject

	base := "$SYS/broker"

	im := &impl{
		SysParams: params,
		config:    cfg,
		quit:      make(chan struct{}),
		tree: tree{
			server:  newServer(base, params.Version, params.BuildTimestamp, &retains, &staticRetains),
			metric:  newMetric(base, &retains),
			clients: newClients(base, &retains),
		},
	}

	for _, d := range retains {
		v := d.(vlmonitoring.DynamicIFace)
		im.publishes = append(im.publishes, v)
	}

	retains = append(retains, staticRetains...)

	for _, o := range retains {
		if err = params.Messaging.Retain(o); err != nil {
			params.Log.Errorf("cannot start retain")
			return nil, err
		}
	}

	im.wgStarted.Add(1)
	im.wgStopped.Add(1)
	im.timer = time.NewTimer(time.Millisecond)

	go im.publishUpdates()

	im.wgStarted.Wait()

	return im, nil
}

func (im *impl) Push(stats vlmonitoring.Stats) {
	im.tree.clients.active.Set(stats.Clients.Connected.Get())
	im.tree.clients.inactive.Set(stats.Clients.Persisted.Get())

	im.tree.clients.connected.Set(stats.Clients.Connected.Get())
	im.tree.clients.disconnected.Set(stats.Clients.Persisted.Get())

	im.tree.clients.maximum.Set(stats.Clients.Total.Max)

	im.tree.clients.total.Set(stats.Clients.Total.Get())

	im.tree.clients.expired.Add(stats.Clients.Expired.Get())

	im.tree.metric.packets.inflight.Set(stats.Packets.UnAckSent.Get() + stats.Packets.UnAckRecv.Get())
	im.tree.metric.packets.retainedCount.Set(stats.Packets.Retained.Get())
	im.tree.metric.packets.retainedMax.Set(stats.Packets.Retained.Max)
	im.tree.metric.packets.storedCount.Set(stats.Packets.Stored.Get())
	im.tree.metric.packets.stored.Set(stats.Packets.Stored.Get())

	im.tree.metric.subscriptionsCount.Set(stats.Subs.Total.Get())
	im.tree.metric.subscriptionsMax.Set(stats.Subs.Total.Max)

	im.tree.metric.bytes.bytes.Sent(stats.Bytes.Sent.Get())
	im.tree.metric.bytes.loadSent.min1.Add(stats.Bytes.Sent.Get())
	im.tree.metric.bytes.loadSent.min5.Add(stats.Bytes.Sent.Get())
	im.tree.metric.bytes.loadSent.min15.Add(stats.Bytes.Sent.Get())
	im.tree.metric.bytes.bytes.Recv(stats.Bytes.Recv.Get())
	im.tree.metric.bytes.loadRecv.min1.Add(stats.Bytes.Recv.Get())
	im.tree.metric.bytes.loadRecv.min5.Add(stats.Bytes.Recv.Get())
	im.tree.metric.bytes.loadRecv.min15.Add(stats.Bytes.Recv.Get())

	im.tree.metric.packets.total.sent.Add(stats.Packets.Total.Sent.Get())
	im.tree.metric.packets.total.recv.Add(stats.Packets.Total.Recv.Get())
	im.tree.metric.packets.loadMessagesSent.min1.Add(stats.Packets.Total.Sent.Get())
	im.tree.metric.packets.loadMessagesSent.min5.Add(stats.Packets.Total.Sent.Get())
	im.tree.metric.packets.loadMessagesSent.min15.Add(stats.Packets.Total.Sent.Get())
	im.tree.metric.packets.loadMessagesRecv.min1.Add(stats.Packets.Total.Recv.Get())
	im.tree.metric.packets.loadMessagesRecv.min5.Add(stats.Packets.Total.Recv.Get())
	im.tree.metric.packets.loadMessagesRecv.min15.Add(stats.Packets.Total.Recv.Get())
	im.tree.metric.packets.loadPublishSent.min1.Add(stats.Packets.Publish.Sent.Get())
	im.tree.metric.packets.loadPublishSent.min5.Add(stats.Packets.Publish.Sent.Get())
	im.tree.metric.packets.loadPublishSent.min15.Add(stats.Packets.Publish.Sent.Get())
	im.tree.metric.packets.loadPublishRecv.min1.Add(stats.Packets.Publish.Recv.Get())
	im.tree.metric.packets.loadPublishRecv.min5.Add(stats.Packets.Publish.Recv.Get())
	im.tree.metric.packets.loadPublishRecv.min15.Add(stats.Packets.Publish.Recv.Get())
}

func (im *impl) Shutdown() error {
	close(im.quit)
	im.wgStopped.Wait()

	if !im.timer.Stop() {
		<-im.timer.C
	}

	return nil
}

func (im *impl) publishUpdates() {
	defer im.wgStopped.Done()
	im.wgStarted.Done()

	for {
		select {
		case <-im.timer.C:
			for _, val := range im.publishes {
				p := val.Publish()
				if p != nil {
					pkt := mqttp.NewPublish(mqttp.ProtocolV311)

					pkt.SetPayload(p.Payload())
					_ = pkt.SetTopic(p.Topic())
					_ = pkt.SetQoS(p.QoS())
					_ = im.Messaging.Publish(pkt)
				}
			}

			if im.config.Interval > 0 {
				im.timer.Reset(time.Duration(im.config.Interval) * time.Second)
			}
		case <-im.quit:
			return
		}
	}
}
