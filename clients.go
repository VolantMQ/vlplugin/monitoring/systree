package main

import (
	"github.com/VolantMQ/vlapi/vlmonitoring"
	"github.com/VolantMQ/vlapi/vltypes"
)

type clients struct {
	active       vlmonitoring.DynamicIFace // deprecated
	inactive     vlmonitoring.DynamicIFace // deprecated
	connected    vlmonitoring.DynamicIFace
	disconnected vlmonitoring.DynamicIFace
	maximum      vlmonitoring.DynamicIFace
	total        vlmonitoring.DynamicIFace
	expired      vlmonitoring.DynamicIFace
}

func newClients(topicPrefix string, retained *[]vltypes.RetainObject) clients {
	c := clients{
		active:       newDynamicValueInteger(topicPrefix + "/clients/active"),
		inactive:     newDynamicValueInteger(topicPrefix + "/clients/inactive"),
		connected:    newDynamicValueInteger(topicPrefix + "/clients/connected"),
		disconnected: newDynamicValueInteger(topicPrefix + "/clients/disconnected"),
		maximum:      newDynamicValueInteger(topicPrefix + "/clients/maximum"),
		total:        newDynamicValueInteger(topicPrefix + "/clients/total"),
		expired:      newDynamicValueInteger(topicPrefix + "/clients/expired"),
	}

	*retained = append(*retained, c.active)
	*retained = append(*retained, c.inactive)
	*retained = append(*retained, c.connected)
	*retained = append(*retained, c.disconnected)
	*retained = append(*retained, c.maximum)
	*retained = append(*retained, c.total)
	*retained = append(*retained, c.expired)

	return c
}
